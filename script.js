var signout = document.querySelector('.signout')
var signIn = document.querySelector('.signin')
var load = document.querySelector('.load')
var avatar = document.querySelector('.avatar')
var fullname = document.querySelector('.fullname')
var country = document.querySelector('.country')
var hobbies = document.querySelector('.hobbies')
var mail = document.querySelector('.mail')
var password = document.querySelector('.password')


signIn.addEventListener('click', function (e) {
  var xhr = new XMLHttpRequest();
  e.preventDefault()
  var body = 'email=' + encodeURIComponent(mail.value) + '&password=' + encodeURIComponent(password.value)

  xhr.open('POST', 'http://netology-hj-ajax.herokuapp.com/homework/login_json')
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')

  xhr.addEventListener('loadstart', function () {
    load.innerHTML = 'loading...'
  })

  xhr.addEventListener('load', function () {
    if (xhr.status >= 200 && xhr.status < 300) {
      var person = JSON.parse(xhr.responseText)

      avatar.src = person.userpic
      avatar.width = 100
      avatar.height = 100

      var name = person.name + ' ' + person.lastname
      fullname.innerHTML = name

      hobbies.innerHTML = 'Hobbies: ' + person.hobbies[0] + ', ' + person.hobbies[1] + ', ' + person.hobbies[2] + ', ' + person.hobbies[3]

      country.innerHTML = person.country
      load.innerHTML = ''
    } else {
      load.innerHTML = 'Loading failed!'
    }
  })

  xhr.addEventListener('error', function () {
    load.innerHTML = 'error №' + xhr.status + ' ' + xhr.statusText
  })

  xhr.send(body)
})

signout.addEventListener('click', function () {
  location.reload()
})
